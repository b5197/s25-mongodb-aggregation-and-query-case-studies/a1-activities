//1st
db.fruits.aggregate([
{$match: {price: {$lt:50},supplier: "Yellow Farms"}},
{$count: "totalNumSupYellowFarms"}
])

//2nd
db.fruits.aggregate([
{$match: {price: {$lt:50}}},
{$count: "totalItemsLt30"}
])

//3rd
db.fruits.aggregate([
{$match: {supplier: "Yellow Farms"}},
{$group: {_id:"avgPriceByYellowFarms",avgPrice:{$avg:"$price"}}}
])

//4th
db.fruits.aggregate([
{$match: {supplier: "Red Farms Inc."}},
{$group: {_id:"highestPriceByRedFarms",maxPrice:{$max:"$price"}}}
])

//5th
db.fruits.aggregate([
{$match: {supplier: "Red Farms Inc."}},
{$group: {_id:"lowestPriceByRedFarms",minPrice:{$min:"$price"}}}
])